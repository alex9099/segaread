--- English ---

This is the code you should upload to the arduino

You should connect the controller like this:

DB9 pin1 -> D2

DB9 pin2 -> D3

DB9 pin3 -> D4

DB9 pin4 -> D5

DB9 pin5 -> 5v

DB9 pin6 -> D6

DB9 pin7 -> D7

DB9 pin8 -> GND

DB9 pin9 -> D8

You can see the controller pinout here: http://www.haku.co.uk/pics/SegaJoypadPinout.jpg

--- Português ---

Este codigo deve ser enviado para o arduino

Deve conectar o controlador ao arduino da seguinte forma:

DB9 pin1 -> D2
DB9 pin2 -> D3
DB9 pin3 -> D4
DB9 pin4 -> D5
DB9 pin5 -> 5v
DB9 pin6 -> D6
DB9 pin7 -> D7
DB9 pin8 -> GND
DB9 pin9 -> D8

Pode ver a pinagem do controlador aqui: http://www.haku.co.uk/pics/SegaJoypadPinout.jpg
