# segaread
SegaRead

--- English ---
This code is intended for you to use a Sega MegaDrive or Sega Genesis on the computer trough a cheap microcontroller, the arduino board, the code is not that complex and has some known issues, let me know if you find any other issue for me to correct

Known Issues:
- Not entering a valid port gives a error
- There is a little bit of input lag, but almost unnoticeable
TODO: 
- Write the code in other client languages and platforms


License: GPLv2

Fell free to fork and contribute to this project :)

--- Português --

Este codigo tem como objetivo usar um controlador de sega megadrive ou sega genesis através de um microcontrolador, a placa arduino, este codigo não é muito complexo e tem alguns bugs, diga se encontrar algum outro bug para tentar corrigir

Bugs conhecidos:
- Não dando uma porta valida o programa termina com uma exceção
TODO:
- Escrever o cliente em outras linguagens e outras plataformas

Licença: GPLv2

Sinta-se livre de contribuir para este projecto
