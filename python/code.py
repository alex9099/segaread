from pymouse import PyMouse
from pykeyboard import PyKeyboard

m = PyMouse()
k = PyKeyboard()

import serial
import sys
port =  raw_input("Please enter the serial port (Ex: Linux: /dev/ttyUSB0; Windows: COM1): ")
ser = serial.Serial(port, 2000000, timeout=1)
items = []

while True:
        readq = ser.readline()
        if (readq == 'UP\n'):
		if ser.readline() == "1\n":
                	k.press_key('w')
		else:
                	k.release_key("w")
        if (readq == 'DOWN\n'):
                if ser.readline() == "1\n":
                        k.press_key('s')
                else:
                        k.release_key("s")
        if (readq == 'LEFT\n'):
                if ser.readline() == "1\n":
                        k.press_key('a')
                else:
                        k.release_key("a")
        if (readq == 'RIGHT\n'):
                if ser.readline() == "1\n":
                        k.press_key('d')
                else:
                        k.release_key("d")
        if (readq == 'A\n'):
                if ser.readline() == "1\n":
                        k.press_key('z')
                else:
                        k.release_key("z")
        if (readq == 'B\n'):
                if ser.readline() == "1\n":
                        k.press_key('x')
                else:
                        k.release_key("x")
        if (readq == 'C\n'):
                if ser.readline() == "1\n":
                        k.press_key('c')
                else:
                        k.release_key("c")
        if (readq == 'START\n'):
                if ser.readline() == "1\n":
                        k.press_key('p')
                else:
                        k.release_key("p")
ser.close()
